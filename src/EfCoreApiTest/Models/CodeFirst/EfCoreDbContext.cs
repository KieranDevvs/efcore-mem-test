﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EfCoreApiTest.Models.CodeFirst
{
    public class EfCoreDbContext : DbContext
    {
        public DbSet<ReadingBatch> ReadingBatches { get; set; }
        public DbSet<Reading> Readings { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=efCoreTest;Integrated Security=true");
        }
    }
}
