﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfCoreApiTest.Models.CodeFirst
{
    public partial class ReadingBatch
    {
        public ReadingBatch()
        {
            Reading = new HashSet<Reading>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime ReadingDateTime { get; set; }
        public DateTime ReceivedDateTime { get; set; }

        public virtual ICollection<Reading> Reading { get; set; }
    }
}
