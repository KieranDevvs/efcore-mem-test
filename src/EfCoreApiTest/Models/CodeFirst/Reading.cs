﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using EfCoreApiTest.Models.CodeFirst;

namespace EfCoreApiTest.Models
{
    public partial class Reading
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public decimal Value { get; set; }
        public bool SomeBool { get; set; }
        public int SomeId { get; set; }
        public long ReadingBatchId { get; set; }

        public virtual ReadingBatch ReadingBatch { get; set; }
    }
}
