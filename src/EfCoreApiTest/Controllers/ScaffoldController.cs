﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfCoreApiTest.Models;
using EfCoreApiTest.Models.CodeFirst;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EfCoreApiTest.Controllers
{
    [Route("api/[controller]")]
    public class ScaffoldController : Controller
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            using (var context = new EfCoreDbContext())
            {

                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                context.Database.ExecuteSqlCommand("alter database EfCoreTest set recovery simple;");

                var LoadBatchesSQL = @"

                                    with N as
                                    (
                                       select top (550000) cast(row_number() over (order by (select null)) as int) i
                                       from sys.objects o, sys.columns c, sys.columns c2
                                    )
                                    insert into ReadingBatches(
                                                                     ReadingDateTime ,
                                                                     ReceivedDateTime)
                                    select '01-01-2017', '01-01-2017'
                                    from N;
                                    ";

                var LoadReadingsSQL = @"

                                        with N as
                                        (
                                           select top (550000) cast(row_number() over (order by (select null)) as int) i
                                           from sys.objects o, sys.columns c, sys.columns c2
                                        )
                                        insert into Readings (
                                                                Value ,
                                                                SomeBool ,
                                                                SomeId ,
                                                                ReadingBatchId)
                                        select 1, 1, 1, i
                                        from N

                                        ";

                Debug.WriteLine("Generating Data ...");
                context.Database.ExecuteSqlCommand(LoadBatchesSQL);
                Debug.WriteLine("Loaded Batches");

                for (var i = 0; i < 3; i++)
                {
                    var rows = context.Database.ExecuteSqlCommand(LoadReadingsSQL);
                    Debug.WriteLine($"Loaded Entity Batch {rows} rows");
                }
                
                Debug.WriteLine("Finished Generating Data");

                var results = context.ReadingBatches.AsNoTracking()
                    .Include(x => x.Reading)
                    .AsEnumerable();

                var batchSize = 10 * 1000;
                var ix = 0;
                foreach (var r in results)
                {
                    ix++;

                    if (ix % batchSize == 0)
                    {
                        Debug.WriteLine($"Read Entity {ix} with name {r.Id}.  Current Memory: {GC.GetTotalMemory(false) / 1024}kb GC's Gen0:{GC.CollectionCount(0)} Gen1:{GC.CollectionCount(1)} Gen2:{GC.CollectionCount(2)}");
                    }

                }
            }
           

            Debug.WriteLine($"Done.  Current Memory: {GC.GetTotalMemory(false) / 1024}kb");
            return new string[] { "value1", "value2" };
        }
    }
}
